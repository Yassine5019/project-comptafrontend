import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../models/client';
import { ClientService } from '../_services/client.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-creer-client',
  templateUrl: './creer-client.component.html',
  styleUrls: ['./creer-client.component.css']
})
export class CreerClientComponent implements OnInit {
  user = this.tokenStorageService.getUser();
  client:Client= new Client()
  isSuccessfulSave=false;
  submitted=false;
  form: any = {
    moisSelected:['', [Validators.required]],
    formJuridiqueSelected:['', [Validators.required]]
  };
  data= [
    {id: 0, name: "Janvier"},
    {id: 1, name: "Fevrier"},
    {id: 2, name: "Mars"},
    {id: 3, name: "Avril"},
    {id: 4, name: "Mai"},
    {id: 5, name: "Juin"},
    {id: 6, name: "Juillet"},
    {id: 7, name: "Aout"},
    {id: 8, name: "Septembre"},
    {id: 9, name: "Octobre"},
    {id: 10, name: "Novembre"},
    {id: 11, name: "Decembre"}
   ];
   dataFormJuridique= [
    {id: 0, name: "SARL"},
    {id: 1, name: "SA"},
    {id: 2, name: "SAS"},
    {id: 3, name: "SCI"},
    {id: 4, name: "BNC"},
    {id: 5, name: "BIC"},
    {id: 6, name: "Association"},
    {id: 7, name: "Autres"}
   ];
  constructor(private clientService:ClientService, private router:Router,
              private tokenStorageService:TokenStorageService) { }

  ngOnInit(): void {
  }

  creerClient(){
    this.submitted=true;
    this.client.mois=this.form.moisSelected
    this.client.formeJuridique=this.form.formJuridiqueSelected
    this.clientService.creerClient(this.client)
    .subscribe(data => {
      this.isSuccessfulSave=true;
      this.client=new Client();
      if(this.user.role.includes('ROLE_ADMIN')){
        this.router.navigate(['admin']);
        } else {
          this.router.navigate(['user']);
        }
    }, error => {
      console.log(error)
    });
  }
  // this.isLoggedIn = !!this.tokenStorageService.getToken();
  // if (this.isLoggedIn) {
  //   const user = this.tokenStorageService.getUser();
  //   this.role = user.role;
  //   this.showAdminBoard = this.role.includes('ROLE_ADMIN');
  //   this.username = user.username;
  // }

  goToList(){
    if(this.user.role.includes('ROLE_ADMIN')){
      this.router.navigate(['admin']);
      } else {
        this.router.navigate(['user']);
      }
  }
}
