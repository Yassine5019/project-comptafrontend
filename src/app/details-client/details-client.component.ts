import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from '../models/client';
import { ClientService } from '../_services/client.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-details-client',
  templateUrl: './details-client.component.html',
  styleUrls: ['./details-client.component.css']
})
export class DetailsClientComponent implements OnInit {
  user=this.tokenStorageService.getUser();
  id!:number
  client!:Client
  constructor(private router:Router, private route:ActivatedRoute, private clientService:ClientService,
              private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.client=new Client();
    this.id=this.route.snapshot.params.id;
    this.clientService.client(this.id).subscribe(data=>{
      this.client=data;
    },
    error=>{
      console.log(error)
    })
  }

  goToList(){
    if(this.user.role.includes('ROLE_ADMIN')){
      this.router.navigate(['admin']);
      } else {
        this.router.navigate(['user']);
      }
  }

}
