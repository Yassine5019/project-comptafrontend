import { AfterViewInit, Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  private role?: any;
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username?: string;

  constructor(private tokenStorageService: TokenStorageService) { }

  ngAfterViewInit(){
   // alert("okkkkkkkkkkkkkk")
  }

  ngOnInit(): void {
    //console.log("LoggedIn" + this.isLoggedIn)
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    // console.log("LoggedIn" + this.isLoggedIn)
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.role = user.role;
      //console.log(this.role)
      this.showAdminBoard = this.role.includes('ROLE_ADMIN');
      //console.log(this.showAdminBoard)
      this.username = user.username;
    }
    //alert("FGeeeeeeeeeeeeeeeeeee")
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}