export class Client{
          public id:number
          public codeClient:string   
          public dateCloture:Date 
          public dateCreation:Date=new Date()
          public formeJuridique:string 
          public mois:string 
          public tenue:string  
          public compteAnnuel:string 
          public date_transmi_CGA:string  
          public juri_depot_compte:string  
          public tva:string  
          public taxe_salaire:string  
          public liasse:string  
          public is:string  
          public cve_1330:string  
          public cve_1329:string  
          public cfe_14_m:string  
          public cfe_14_c:string  
          public cfe_accompte:string  
          public cfe_solde:string  
          public ch_2777_d:string  
          public ifu:string  
          public ta:string  
          public fpc:string  
          public ec:string  
          public c3s:string  
          public tvts:string  
          public dcr:string  
          public nomClient:string  
          public user:{
               user_id:number,
               username:string
          }
     constructor(){}
 }
