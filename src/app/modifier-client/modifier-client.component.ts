import { Component, OnInit } from '@angular/core';
import { Client } from '../models/client';
import { ClientService } from '../_services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-modifier-client',
  templateUrl: './modifier-client.component.html',
  styleUrls: ['./modifier-client.component.css']
})
export class ModifierClientComponent implements OnInit {
  user = this.tokenStorageService.getUser();
  id:number;
  client:Client;
  submitted=false;
  isSuccessfulUpdate=false;
  data= [
    {id: 0, name: "Janvier"},
    {id: 1, name: "Fevrier"},
    {id: 2, name: "Mars"},
    {id: 3, name: "Avril"},
    {id: 4, name: "Mai"},
    {id: 5, name: "Juin"},
    {id: 6, name: "Juillet"},
    {id: 7, name: "Aout"},
    {id: 8, name: "Septembre"},
    {id: 9, name: "Octobre"},
    {id: 10, name: "Novembre"},
    {id: 11, name: "Decembre"}
   ];
  form: any = {
    moisSelected:['', [Validators.required]]
  };
  constructor(private clientService:ClientService, private router:Router,
              private route:ActivatedRoute, private tokenStorageService : TokenStorageService) { }

  ngOnInit(): void {
    this.client=new Client();
    this.id=this.route.snapshot.params.id;
    this.clientService.client(this.id).subscribe(data=>{
      this.client=data;
    },
    error=>{
      console.log(error)
    })

  }

  goToList(){
    if(this.user.role.includes('ROLE_ADMIN')){
      this.router.navigate(['admin']);
      } else {
        this.router.navigate(['user']);
      }
   }

  modifierClient(){
    this.submitted=true;
    this.clientService.modifierClient(this.client, this.id).subscribe(data=>{
      this.isSuccessfulUpdate=true;
      this.client=new Client();  
      if(this.user.role.includes('ROLE_ADMIN')){
        this.router.navigate(['admin']);
        } else {
          this.router.navigate(['user']);
        }
    },
    error => {
      console.log(error)
    });
    console.log("okk")
  }

}
