import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';
import { Client } from '../models/client'
import { FormControl } from '@angular/forms';
import { ClientService } from '../_services/client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit{
  client:Client
  clients:Client[];
  clientVal:FormControl; 
  subbmitedMois:boolean;
  selectedRow:number;
  champColl:string
  currentPage:string;
  moisName:string
  moisSelected:any;
  data= [
    {id: 0, name: "Janvier"},
    {id: 1, name: "Fevrier"},
    {id: 2, name: "Mars"},
    {id: 3, name: "Avril"},
    {id: 4, name: "Mai"},
    {id: 5, name: "Juin"},
    {id: 6, name: "Juillet"},
    {id: 7, name: "Aout"},
    {id: 8, name: "Septembre"},
    {id: 9, name: "Octobre"},
    {id: 10, name: "Novembre"},
    {id: 11, name: "Decembre"}
   ];

  constructor(private userService: UserService, private tokenStorage : TokenStorageService,
              private clientService : ClientService, private router:Router) { }

  ngOnInit(): void {
    this.clientVal=new FormControl();
    //this.reloadClientsById()
    //console.log("Le mois selectionné : " + this.moisSelected)
    //console.log("kkkkkkkkkkkkkkkkk : " + this.onMonthChange('Janvier'))
  }

  ClickedRow(i:number){
   this.selectedRow=i
  }

  ClickedColl(champ:string){
      this.champColl = champ;
  }

  onMonthChange(mois:any){
    const id=this.tokenStorage.getUser().id
    this.userService.getUserBoard(mois,id).subscribe(
      data => {
        this.clients = data
        this.subbmitedMois=true
        
      },
      err => {
        this.clients = JSON.parse(err.error).message;
      }
    );
  }

  ConvertToJSON(client: any) {
    if(typeof client !== 'undefined'){
      return JSON.parse(client);
    }
  }

  onSelectedRow(client:Client){
      const newDate = JSON.parse(JSON.stringify(new Date().toLocaleString('fr-FR', {day: '2-digit', month: '2-digit', year: '2-digit'})));
      if(client.tenue==='0'){
        if(confirm("Voulez-vous vraiement changer la couleur !! ")){
          client.tenue="1"
          this.clientService.modifierClient(client, client.id).subscribe(data=>{
                                         this.onMonthChange(this.moisSelected)
                                 },
                      error=>{
                        console.log(error)
                      })
          }
                }else if(client.tenue==='1'){ 
                      if(confirm("Voulez-vous vraiement valider la création !! ")){
                        client.tenue=newDate
                        this.clientService.modifierClient(client, client.id).subscribe(data=>{
                        this.onMonthChange(this.moisSelected)
                        return this.clients
                        },
                        error=>{
                          console.log(error)
                        })
                      }
                          } else{
                            client.tenue="0"
                            this.clientService.modifierClient(client, client.id).subscribe(data=>{
                              this.onMonthChange(this.moisSelected)
                            return this.clients
                            },
                            error=>{
                              console.log(error)
                            })
                  }
  }

  modifierClient(id:number){
    this.router.navigate(['modifier-client', id]);
  }

  detailsClient(id:number){
    this.router.navigate(['details-client', id]);
  }

  supprimerClient(id:number){
   if(confirm("Voulez-vous vraiment faire la suppression !! ")){
    this.clientService.supprimerClient(id).subscribe(
      data =>{
      this.onMonthChange(this.moisSelected)
      }, error =>{
        console.log(error)
      }
    )
   }
  }

  parseData(data:any) {
    if (!data) return {};
    if (typeof data === 'object') return data;
    if (typeof data === 'string') return JSON.parse(data);

    return {};
  }
}