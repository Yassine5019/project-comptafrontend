import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

 private baseUrl='http://localhost:8080/api/test'
  constructor(private httpService:HttpClient) { }

  creerClient(client : Client) : Observable<any>{
     //return this.httpService.post(this.baseUrl + '/saveClient', client);
     return this.httpService.post(`${this.baseUrl}/client`, client);
  }

  modifierClient(client : Client, id:any) : Observable<any>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.put(`${this.baseUrl}/client/${id}`, client);
  }

  supprimerClient(id:any) : Observable<any>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.delete(`${this.baseUrl}/client/${id}`);
  }

  clients() : Observable<Client[]>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.get<Client[]>(`${this.baseUrl}/admin`);
  }

  clientByMonth(monthSelected:string) :  Observable<any>{
    let paramsMonth=new HttpParams().set('moisId', monthSelected)
    const httpClient=this.httpService.get<Client[]>(`${this.baseUrl}/clients/${monthSelected}`, {params:paramsMonth});
    return httpClient;
  }

  client(id:any) : Observable<any>{
    //return this.httpService.get(`${this.baseUrl}/client/${id}`); hhhhhhhhhhhhhhhhhhhhhhhhhhhhh
    return this.httpService.get(`${this.baseUrl}/client/${id}`).pipe(
      map(res => res as Client[] || [])
    );
  }

}
