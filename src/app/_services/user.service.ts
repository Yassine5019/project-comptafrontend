import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client'

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(mois:string, id:number): Observable<any> {
    let paramsMonth=new HttpParams().set('moisId', mois)
    return this.http.get(API_URL + 'user/' + id + '/' + mois, { responseType: 'text', params:paramsMonth });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  // getAdminBoard(): Observable<Client[]> {
  //   return this.http.get<Client[]>(API_URL + 'admin');
  // }

  getAdminBoardByMonth(mois:string): Observable<Client[]> {
    return this.http.get<Client[]>(API_URL + 'clients/' + mois);
  }

}
