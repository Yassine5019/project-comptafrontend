Open `app/_helpers/auth.interceptor.js`, modify the code to work with **x-access-token** like this:
```js

const TOKEN_HEADER_KEY = 'x-access-token';  

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  ...

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    ...
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, token) });
    }
    return next.handle(authReq);
  }
}